#!/usr/bin/python3
import os
import signal
import subprocess
import threading
import multiprocessing
from queue import Queue
import time

# lock to serialize console output
lock = threading.Lock()

def do_work(item):
    time.sleep(.1) # pretend to do some lengthy work.

    

    with lock:
        # how todo a good system call
        # subprocess.check_call(['ls', '*']
        command = subprocess.run("exit 1", shell=True, check=False)
        print (returncode)
        print(threading.current_thread().name,item)
        # print the number of cpus
        #print(multiprocessing.cpu_count())
        #os.kill(os.getpid(), signal.SIGINT)

# The worker thread pulls an item from the queue and processes it
def worker():
    while True:
        item = q.get()
        do_work(item)
        q.task_done()

# Create the queue and thread pool.
q = Queue(4)
for i in range(20):
     t = threading.Thread(target=worker)
     t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
     t.start()

# stuff work items on the queue (in this case, just a number).
start = time.perf_counter()
for item in range(32):
    q.put(item)

q.join()       # block until all tasks are done